# speedie's zsh configuration
# https://github.com/speedie-de/szsh

# Features
autoload -U colors && colors
autoload -U compinit promptinit
autoload -Uz compinit
command -v emerge > /dev/null && promptinit; prompt gentoo
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion:*' menu select
zmodload
compinit
_comp_options+=(globdots)

# Settings
STARSHIP=false
POWERLINE=true

# Source other dotfiles
source $ZDOTDIR/.zsh_export
source $ZDOTDIR/.zsh_ps
source $ZDOTDIR/.zsh_func
source $ZDOTDIR/.zsh_alias

# Plugins
source $ZPLUGINDIR/zsh-vi-mode/zsh-vi-mode.zsh
source $ZPLUGINDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZPLUGINDIR/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZPLUGINDIR/zsh-titles/zsh-titles.zsh

#######################################################

command -v starship > /dev/null || update_starship
[ "$STARSHIP" = "true" ] && eval "$(starship init zsh)" && write_starship_config

cd && clear

command -v "$FETCH" > /dev/null && $FETCH
