#!/bin/sh

if [ -e "$HOME/.config/zsh" ]; then
    rm -rf .config/zsh
    cp -r $HOME/.config/zsh .config/

    # remove history
    rm -rf .config/zsh/.zcompcache
    rm -rf .config/zsh/.zcompdump
    rm -rf .config/zsh/history
    rm -rf .config/zsh/dotfiles/history

    git add .config/* install.sh commit.sh
    git commit -a -m "speedie-zsh | Add new config"
    git push
fi
